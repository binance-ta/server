# Cryptocurrency Technical Analysis - Server
Cryptocurrency Technical Analysis server is a simple local server that can provide you with cryptocurrency exchanges rest apis & their websocket data stream (if exchange supports wss). The goal is to provide user with "live" data.
<br/><br/>

## Table of contents
1. [Disclaimer](#Disclaimer)
2. [Get started](#Getstarted)
3. [Install](#Install)
4. [Usage](#Usage)
5. [Functionality](#Functionality)
6. [Contribute](#Contribute)
7. [Coverage](#Coverage)
8. [License](#License)
<br/>

## Disclaimer
All of my code and my knowledge is part of a hobby. I have minimal/no experience in other languages than javascript/node. I am not responsible for any of your interaction with the application. Usage at your own risk.
<br/>

## Get started
For interacting with 100% of the server functionality, you will need following API keys:
* Binance - Testnet or Mainnet

Frontend repository: [https://gitlab.com/binance-ta/frontend][1]

[1]: https://gitlab.com/binance-ta/frontend
<br/>

## Install
C&P the following commands:
```bash
git clone https://gitlab.com/binance-ta/server.git
cd server
npm i
```
- Clone the frontend repo, copy the path and paste it to the FRONTEND env. variable.
```bash
git clone https://gitlab.com/binance-ta/frontend.git
cd frontend
npm i
npx tsc
```
<br/>

- Create a .env file, change variables and save it.
```bash
# Set to production when deploying to production
NODE_ENV=development
# Hide docs in production mode
API_DOCS=https://app.swaggerhub.com/apis/KKA11010/binance-TA-server/0.0.0
# Set path to frontend dir
FRONTEND=C:\dir_to_the_frontend

# Node.js server configuration
SERVER_HOST=https://localhost
SERVER_PORT=8443

# testnet HMAC_SHA256 key
TESTNET_API_KEY=your_testnet_api_key
TESTNET_SECRET_KEY=your_testnet_secret_key

# mainnet key
API_KEY=your_live_api_key
SECRET_KEY=your_live_secret_key
```

- Start the ssl.sh script located in the ssl folder that creates a self-signed certificate to communicate via https.


- Ready to go. Open bash in the root dir and run:
```bash
npm run start
```

<br/>

## Usage
After successfull start of the server, browse to "https://localhost:8443/" and start exploring.

<br/>

## Functionality
* Test
  * Test
  * Test
* Test
* Test
* Test
* ...

<br/>

## Contribute
Pull requests are always welcome! For bigger changes, please open an issue first to discuss the topic.

<br/>

## Coverage
Test

<br/>

## License
Test