// c&p from https://basarat.gitbook.io/typescript/intro-1/jest

// export default results in an error thrown from jest
module.exports = {
	'roots': [
		'<rootDir>/src'
	],
	'testMatch': [
		'**/__tests__/**/*.+(ts|tsx|js)',
		'**/?(*.)+(spec|test).+(ts|tsx|js)'
	],
	'transform': {
		'^.+\\.(ts|tsx)$': 'ts-jest'
	},
}