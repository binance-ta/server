import {
	getAllOrders,
	getBalances,
	getDepositAddress,
	// getFees,
	getOpenOrders,
	getPrice,
	getTradeHistory,
	getDailyStats,
	getBalsOnOder,
	getCandleData
} from '../externalApis/exchanges/binance/getApis'
import { cancelAllOrders, cancelOrderId, placeOrder } from '../externalApis/exchanges/binance/postApis'
import { client } from '../externalApis/exchanges/binance/lib'
import { config } from 'dotenv'
import express from 'express'
import { getAddressInfos, getAddressTxs, getBlockByHeight, getBlockListByDate, getBlockWhereDiffRetargets, getHashesNeededToWin, getLatestBlock, getLatestBlockTransactions, getOneHashProbability, getTotalBtc, getTxInfosByTxHash } from '../externalApis/chainData/btc'

export const router = express.Router()

// .env config
config()

const getPrefix = '/api/binance/get'
const postPrefix = '/api/binance/post'
const badResp = {response: 'data not available, check url params'}

// TODO add proper request and error checks in all routes
// TODO add comments
/////////////////////////////////////////////////////////////////////////////// binance ///////////////////////////////////////////////
/* GET */
router.get('/', (_req, res) => { res.sendFile(`${process.env.FRONTEND}/index.html`) })

// ping local server
router.get('/api/ping/local', (_req, res) => {
	console.log('ping')
	res.send({response: 'pong'})
})

// ping binance server
router.get('/api/ping/binance', (_req, res) => res.send(client.ping()))

router.get(`${getPrefix}/price/:pair`, async (req, res) => {
	if (req.params.pair.length) {
		const price = await getPrice(req.params.pair)
		res.send(price)
		return
	}
	res.send(badResp)
})

router.get(`${getPrefix}/24change/:pair`, async (req, res) => {
	if (req.params.pair.length) {
		const stats = await getDailyStats(req.params.pair)
		res.send(stats)
		return
	}
	res.send(badResp)
})

router.get(`${getPrefix}/balances`, async (_req, res) => {
	const balances = await getBalances()
	res.send(balances)
})

router.get(`${getPrefix}/balances/onOrder`, async (_req, res) => {
	const onOrder = await getBalsOnOder()
	res.send(onOrder)
})

router.get(`${getPrefix}/orders/history/:pair`, async (req, res) => {
	if (req.params.pair.length) {
		const history = await getTradeHistory(req.params.pair)
		res.send(history)
		return
	}
	res.send(badResp)
})

router.get(`${getPrefix}/orders/open/:pair`, async (req, res) => {
	if (req.params.pair.length) {
		const orders = await getOpenOrders(req.params.pair)
		res.send(orders)
		return
	}
	res.send(badResp)
})

router.get(`${getPrefix}/candles/:pair/:interval/:limit`, async (req, res) => {
	if (req.params.pair && req.params.interval && req.params.limit) {
		const data = await getCandleData(req.params.pair, req.params.interval, req.params.limit)
		res.send(data)
		return
	}
	return badResp
})

router.get(`${getPrefix}/orders/all/:pair`, async (req, res) => {
	if (req.params.pair.length) {
		const all = await getAllOrders(req.params.pair)
		res.send(all)
		return
	}
	res.send(badResp)
})

router.get(`${getPrefix}/address/:asset`, async (req, res) => {
	if (req.params.asset.length) {
		const address = await getDepositAddress(req.params.asset)
		res.send(address)
		return
	}
	res.send(badResp)
})

/* router.get(`${getPrefix}/fees/:pair`, async (req, res) => {
	if (req.params.pair.length) {
		const fees = await getFees(req.params.pair)
		res.send(fees)
		return
	}
	return badResp
}) */

/* POST */
router.post(`${postPrefix}/orders/cancel`, async (req, res) => {
	if (!req.body.id) {
		const cancelled = await cancelAllOrders(req.body.pair)
		res.send(cancelled)
		return
	}
	const cancelledId = await cancelOrderId(req.body.pair, req.body.id)
	res.send(cancelledId)
})

router.post(`${postPrefix}/orders/place`, async (req, res) => {
	const pair = req.body.pair
	const side = req.body.side
	const qty = req.body.qty
	const type = req.body.type
	const price = req.body.price
	const resp = await placeOrder(pair, side, qty, type, price)
	res.send(resp)
})

/////////////////////////////////////////////////////////////////////////////// blockchain info ///////////////////////////////////////////////

const chainPrefix = '/api/chain/btc'

router.get(`${chainPrefix}/total`, (_req, res) => { void getTotalBtc(res) })

router.get(`${chainPrefix}/probability`, (_req, res) => { void getOneHashProbability(res) })

router.get(`${chainPrefix}/hashestowin`, (_req, res) => { void getHashesNeededToWin(res) })

router.get(`${chainPrefix}/nextretarget`, (_req, res) => { void getBlockWhereDiffRetargets(res) })

router.get(`${chainPrefix}/block/:height`, (req, res) => {
	if (req.params.height !== '') {
		void getBlockByHeight(req.params.height, res)
		return
	}
	res.send({response: 'data not available'})
})

router.get(`${chainPrefix}/block/latest`, (_req, res) => {
	void getLatestBlock(res)
	// res.send({response: 'data not available'})
})

router.get(`${chainPrefix}/block/date/:date`, (req, res) => {
	if (req.params.date.length === 8) {
		void getBlockListByDate(req.params.date, res)
		return
	}
	res.send({response: 'data not available'})
})

router.get(`${chainPrefix}/block/latest/tx`, (_req, res) => {
	void getLatestBlockTransactions(res)
})

router.get(`${chainPrefix}/address/:address`, (req, res) => {
	if (req.params.address !== '') {
		void getAddressInfos(req.params.address, res)
		return
	}
	res.send({response: 'data not available'})
})

router.get(`${chainPrefix}/address/tx/:address`, (req, res) => {
	if (req.params.address !== '') {
		void getAddressTxs(req.params.address, res)
		return
	}
	res.send({response: 'data not available'})
})

router.get(`${chainPrefix}/tx/:hash`, (req, res) => {
	if (req.params.hash !== '') {
		void getTxInfosByTxHash(req.params.hash, res)
		return
	}
	res.send({response: 'data not available'})
})