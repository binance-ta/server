// main api response /////////////////////
export interface IBlockListResp {
	data: IBlock[],
	err_code: number,
	err_no: 0 | 1 | 2,
	message: string,
	status: string
}
export interface IBlockResp {
	data: IBlock,
	err_code: number,
	err_no: 0 | 1 | 2,
	message: string,
	status: string
}
export interface ITransactionsList {
	data: {
		list: ITransaction[]
		page: number
		page_size: number
		page_total: number
		total_count: number
	}
	err_code: number,
	err_no: number,
	message: string,
	status: string
}
export interface ITransactionInfos {
	data: ITransaction
	err_code: number,
	err_no: number,
	message: string,
	status: string
}
export interface IAddressInfosResp {
	data: IAddressInfos
	err_code: number,
	err_no: number,
	message: string,
	status: string
}

// sub interfaces for api response /////////////////////////
// block
export interface IBlock {
	height: number
    version: number
    mrkl_root: string
    curr_max_timestamp: number
    timestamp: number
    bits: number
    nonce: number
    hash: string
    prev_block_hash: string | number // | null
    next_block_hash: string | number // | null
    size: number
    pool_difficulty: number
    difficulty: number
    tx_count: number
    reward_block: number
    reward_fees: number
    created_at: number
    confirmations: number
    extras: {
        pool_name: string,
		pool_link: string
    }
}

// tx
export interface IInputs {
	prev_addresses: string[]
	prev_position: number
	prev_tx_hash: string
	prev_value: number
	script_asm: string
	script_hex: string
	sequence: number
}
export interface IOutput {
	addresses: string[]
    value: number
}
export interface ITransaction {
    block_height: number
    block_time: number
    created_at: number
    fee: number
    hash: string
    inputs: IInputs[]
    inputs_count: number
    inputs_value: number
    is_coinbase: boolean
    lock_time: number
    outputs: IOutput[],
    outputs_count: number
    outputs_value: number
    size: number
    version: number
}

// address
export interface IAddressInfos {
	address: string
	received: number
	sent: number
	balance: number
	tx_count: number
	unconfirmed_tx_count: number
	unconfirmed_received: number
	unconfirmed_sent: number
	unspent_tx_count: number
}