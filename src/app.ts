import express from 'express'
import cors from 'cors'
// import { openSocket } from './externalApis/exchanges/binance/websockets'
import { readFileSync } from 'fs'
import { createServer } from 'https'
import { config } from 'dotenv'
import { join } from 'path'
import { json, urlencoded } from 'body-parser'
import { router } from './middleware/router'
import { updateRecord } from './vps/update'

process.on('uncaughtException', err => {
	console.error(err, 'uncaughtException')
	process.exit(1)
})

process.on('unhandledRejection', err => {
	console.error(err, 'unhandledRejection')
	process.exit(1)
})

// dotenv
config()

// setup
const host = process.env.SERVER_HOST
const port = process.env.SERVER_PORT
const appMode = process.env.NODE_ENV
const app = express()
app.use(urlencoded({ extended: false }))
app.use(json())
app.use(cors())
app.use(router)
// app use frontend
if (process.env.FRONTEND && process.env.FRONTEND !== '') {
	app.use(express.static(join(process.env.FRONTEND)))
}
// app.use(express.json())
// app.use(express.urlencoded({extended: false}))

// ssl
const sslKey = readFileSync('ssl/key.pem', 'utf8')
const sslCert = readFileSync('ssl/cert.pem', 'utf8')

// create server
const httpsServer = createServer({ key: sslKey, cert: sslCert }, app)
/* httpsServer.on('upgrade', function upgrade(request, socket, head) {
	const webSocket = openSocket(request.url || '')
	webSocket.handleUpgrade(request, socket, head, function done(ws) {
		webSocket.emit('connection', ws, request)
	})
}) */

// start server
httpsServer.listen(port, () => {
	const fe = process.env.FRONTEND !== '' ? process.env.FRONTEND : 'Not provided. Setup -.env file & restart.'
	console.log(
		`\nAPI documentation: ${process.env.API_DOCS}\n\n${host}:${port} | ${appMode} mode | Frontend: ${fe}\n`
	)
	// 20 mins interval for update record clubs
	setInterval(() => { updateRecord() }, 1000 * 60 * 20)
	// resetRedis()
})
