import fetch from 'node-fetch'

export async function fetchUrl(url: string) {
	try {
		const response = await fetch(url)
		const json = await response.json()
		console.log(json)
		return json
	} catch (error) {
		console.error(error)
	}
	return null
}
