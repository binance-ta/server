/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/ban-types */

export function getRandomInt(min: number, max: number) { return Math.floor(Math.random() * max) + min }
// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
export const isArr = (v: any): v is any[] => !!((v && Array.isArray(v)) || (v && typeof v === 'object' && v.constructor === Array))
export const isBool = (v: any): v is boolean => typeof v === 'boolean'
// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
export const isErr = (v: any): v is Error => !!(v instanceof Error || (v && v.__proto__ && v.__proto__.name === 'Error'))
// tslint:disable-next-line: ban-types
export const isFunc = (v: any): v is Function => typeof v === 'function'
export const isNull = (v: any): v is null => v === null
export const isNum = (v: any): v is number => typeof v === 'number' && !isNaN(v) && isFinite(v)
// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
export const isObj = (v: any): v is object => !!(v && typeof v === 'object' && v.constructor === Object)
export const isStr = (v: any): v is string => typeof v === 'string' || v instanceof String
export const isUndef = (v: any): v is undefined => typeof v === 'undefined'