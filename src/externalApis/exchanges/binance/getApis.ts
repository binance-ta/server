import { AssetBalance, CandleChartInterval } from 'binance-api-node'
import { binance, client } from './lib'

// TODO add general proper response wrap with resp code
const badResp = {response: 'data not available'}

// Get the price for any coin-pair
export async function getPrice(pair: string) {
	try {
		const price = await client.prices({symbol: pair.toUpperCase()})
		if (!price) {
			console.error('Price: ', price)
			return badResp
		}
		console.log('Price: ', price)
		return price
	} catch(e) {
		console.error(e)
		return badResp
	}
}

// Get 24h stats for any coin-pair
export async function getDailyStats(pair: string) {
	try {
		const stats = await client.dailyStats({symbol: pair.toUpperCase()})
		if (!stats) { 
			console.error('Price: ', stats)
			return badResp
		}
		console.log('Price: ', stats)
		return stats
	} catch(e) {
		console.error(e)
	}
	return badResp
}

// Get a list of available balances for all coins. // TODO frontend authentication?
export async function getBalances() {
	try {
		const info = await binance.accountInfo()
		if (!info) {
			console.error('Account infos: ', info)
			return badResp
		}
		const posBalances = filterZeroBal(info.balances)
		console.log('Account balances: ', posBalances)
		return posBalances
	} catch(e) {
		console.error(e)
	}
	return badResp
}

export async function getBalsOnOder() {
	try {
		const info = await binance.accountInfo()
		if (!info) {
			console.error('Account infos: ', info)
			return badResp
		}
		const posBalances = filterZeroBal(info.balances)
		const onOrder = posBalances.filter(bal => +bal.locked > 0)
		console.log(onOrder)
		return onOrder
	} catch(e) {
		console.error(e)
	}
	return badResp
}

// Get a list of open orders for all coins. // TODO frontend authentication?
export async function getOpenOrders(pair: string) {
	try {
		const orders = await binance.openOrders({symbol: pair.toUpperCase()})
		if (!orders) {
			console.error(`Open orders for ${pair}: `, orders)
			return badResp
		}
		console.log(`Open orders for ${pair}: `, orders)
		return orders
	} catch(e) {
		console.error(e)
	}
	return badResp
}

// Get a trade history list for a coin-pair. // TODO frontend authentication?
export async function getTradeHistory(pair: string) {
	try {
		const history = await binance.myTrades({ symbol: pair.toUpperCase() })
		if (!history) {
			console.error(`Trade history for ${pair}: `, history)
			return badResp
		}
		console.log(`Trade history for ${pair}: `, history)
		return history
	} catch(e) {
		console.error(e)
	}
	return badResp
}

// get deposit address for a specific coin // TODO frontend authentication?
export async function getDepositAddress(asset: string) {
	try {
		const address = await binance.depositAddress({ asset: asset.toUpperCase() })
		console.log(address)
		return address
	} catch(e) {
		console.error(e)
	}
	return badResp
}

// Get a list of all orders for a coin-pair; active, canceled or filled. // TODO frontend authentication?
export async function getAllOrders(pair: string) {
	try {
		const all = await binance.allOrders({symbol: pair.toUpperCase()})
		if (!all) {
			console.error(`All trades for ${pair}: `, all)
			return badResp
		}
		console.log(`All trades for ${pair}: `, all)
		return all
	} catch(e) {
		console.error(e)
	}
	return badResp
}

// Get candles
export async function getCandleData(pair: string, int: string, limit: string) {
	try {
		const data = await client.candles({symbol: pair.toUpperCase(), interval: Object(CandleChartInterval)[int], limit: +limit})
		if (!data) {
			console.error(`All trades for ${pair}: `, data)
			return badResp
		}
		const cleanData = []
		for (const entry of data) {
			cleanData.push({
				x: entry.openTime,
				y: [+entry.open, +entry.high, +entry.low, +entry.close]
			})
		}
		return cleanData
	} catch(e) {
		console.error(e)
	}
	return badResp
}

// Get trading fees // TODO frontend authentication
/* export async function getFees(pair: string) {
	try {
		const resp = await binance.tradeFee()
		if (!resp.success) {
			console.error(`Fees not available for ${pair}`)
			return badResp
		}
		const reqFee = resp.tradeFee.filter(entry => entry.symbol === pair.toUpperCase())
		console.log(`Fees for ${pair}: `, reqFee)
		return reqFee
	} catch(e) {
		console.error(e)
	}
	return badResp
} */

function filterZeroBal(balances: AssetBalance[]): AssetBalance[] {
	return balances.filter(bal => +bal.free > 0 || +bal.locked > 0)
}

// test
export function sum (...a: number[]):number { return a.reduce((acc, val) => acc + val, 0) }
