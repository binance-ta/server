// https://github.com/binance-exchange/binance-api-node
import Binance from 'binance-api-node'
import { config } from 'dotenv'

config()

// Public REST
export const client = Binance()

// Private REST
export const binance = Binance({
	apiKey: process.env.API_KEY ? process.env.API_KEY : '',
	apiSecret: process.env.SECRET_KEY ? process.env.SECRET_KEY : ''
})
