import { Server } from 'ws'
import { binance } from './lib'

export function openSocket(path: string) {
	console.log(`Opening ${path} WebSocket`)
	const wss = new Server({ noServer: true })
	const params = getWsPathParams(path) // example: ['trades', 'btc&usd']
	const pair = params[1].split('&')
	switch (params[0]) {
		case 'trades': return tradeSocket(wss, pair)
		case '24change': return prevDaySocket(wss, pair)
		case 'depths': return depthSocket(wss, pair)
		case 'candles': return candleSocket(wss, pair, params[2])
		case 'balance': return balanceSocket(wss)
		default: break
	}
	return wss
}

function tradeSocket(wss: Server, pair: string[]) {
	// wss://localhost:8443/trades/btc&usdt
	wss.on('connection', function connection(ws) {
		// open binance trade socket
		const clean = binance.ws.trades(`${pair[0].toUpperCase()}${pair[1].toUpperCase()}`, trade => {
			ws.send(JSON.stringify(trade))
		})
		ws.on('message', function incoming(data) {
			console.log('\n' + data)
			if (data === 'destroy') { clean(); wss.close() }
		})
		ws.on('close', function (reasonCode, description) {
			console.log('\nTrade socket disconnected. code: ', reasonCode, description, '\n')
		})
	})
	wss.on('close', () => console.log('\n' + 'Closing Trade socket...'))
	return wss
}

function prevDaySocket(wss: Server, pair: string[]) {
	// wss://localhost:8443/24change/btc&usdt
	wss.on('connection', function connection(ws) {
		// open binance 24h change socket
		const clean = binance.ws.ticker(`${pair[0].toUpperCase()}${pair[1].toUpperCase()}`, change => {
			ws.send(JSON.stringify(change))
		})
		ws.on('message', function incoming(data) {
			console.log('\n' + data)
			if (data === 'destroy') { clean(); wss.close() }
		})
		ws.on('close', function (reasonCode, description) {
			console.log('24h change socket disconnected. code: ', reasonCode, description)
		})
	})
	wss.on('close', () => console.log('\n' + 'Closing Trade socket...'))
	return wss
}

function depthSocket(wss: Server, pair: string[]) {
	// wss://localhost:8443/depths/btc&usdt
	wss.on('connection', function connection(ws) {
		const clean = binance.ws.depth(`${pair[0].toUpperCase()}${pair[1].toUpperCase()}`, depth => {
			ws.send(JSON.stringify(depth))
		})
		ws.on('message', function incoming(data) {
			console.log('\n' + data)
			if (data === 'destroy') { wss.close(); clean() }
		})
		ws.on('close', function (reasonCode, description) {
			console.log('Depth socket disconnected. code: ', reasonCode, description)
		})
	})
	wss.on('close', () => console.log('\n' + 'Closing Trade socket...'))
	return wss
}

function candleSocket(wss: Server, pair: string[], period: string) {
	// wss://localhost:8443/candles/btc&usdt/1h
	// Periods: 1m,3m,5m,15m,30m,1h,2h,4h,6h,8h,12h,1d,3d,1w,1M
	wss.on('connection', function connection(ws) {
		const clean = binance.ws.candles(`${pair[0].toUpperCase()}${pair[1].toUpperCase()}`, period, candle => {
			ws.send(JSON.stringify(candle))
		})
		ws.on('message', function incoming(data) {
			console.log(data)
			if (data === 'destroy') { wss.close(); clean() }
		})
		ws.on('close', function (reasonCode, description) {
			console.log('Candle socket disconnected. code: ', reasonCode, description)
		})
	})
	wss.on('close', () => console.log('\n' + 'Closing Trade socket...'))
	return wss
}

function balanceSocket(wss: Server) {
	// wss://localhost:8443/bookticker/btc&usdt
	wss.on('connection', function connection(ws) {
		binance.ws.user(msg => {
			// console.log(res)
			ws.send(JSON.stringify(msg))
		})
		ws.on('message', function incoming(data) {
			console.log(data)
			if (data === 'destroy') { wss.close() }
		})
		ws.on('close', function (reasonCode, description) {
			console.log('bookticker socket disconnected. code: ', reasonCode, description)
		})
	})
	wss.on('close', () => console.log('\n' + 'Closing Trade socket...'))
	return wss
}

// path example: "/candles/btc&usdt/1h" returns ['candles', 'btc&usdt', '1h']
function getWsPathParams(path: string): string[] {
	const splitted = path.split('/')
	splitted.shift()
	return splitted
}