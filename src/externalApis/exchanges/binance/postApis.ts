import { OrderSide, OrderType } from 'binance-api-node'
import { binance } from './lib'

// TODO add try catches and error handling.
const badResp = {response: 'not available'}

// Cancel all open orders from a coin pair
export async function cancelAllOrders(pair: string) {
	// check if symbol has open orders, otherwise the app crashes
	console.log(`canceling order(s) from ${pair}`)
	try {
		const cancelled = await binance.cancelOpenOrders({ symbol: pair.toUpperCase(), useServerTime: true })
		console.log('canceled order(s): ', cancelled)
		return cancelled
	} catch (error) {
		console.error(error)
	}
	return badResp
}

// Cancel a specific order by id from a coin pair
export async function cancelOrderId(pair: string, id: number) {
	console.log(`canceling order from ${pair} with id: ${id}`)
	try {
		const cancelled = await binance.cancelOrder({symbol: pair.toUpperCase(), orderId: id, useServerTime: true})
		console.log('canceled order(s): ', cancelled)
		return cancelled
	} catch (error) {
		console.error(error)
	}
	return badResp
}

// Placing an order
export async function placeOrder(
	pair: string,
	side: OrderSide,
	qty: string,
	type: OrderType,
	price: string
) {
	try {
		const resp = await binance.order({
			symbol: pair.toUpperCase(),
			side: side,
			quantity: qty,
			type: type,
			price: price
		})
		console.log(resp)
		return resp
	} catch (error) {
		console.error(error)
	}
	return badResp
}
