// source: https://btc.com/api-doc

import { IAddressInfosResp, IBlockListResp, IBlockResp, ITransactionInfos, ITransactionsList } from '../../models/interfaces/btcDotCom'
import { fetchUrl } from '../../nodeFetch'
import { Response } from 'express'
import { isNum } from '../../helper/helper'

const chainApiBase = 'https://chain.api.btc.com/v3/'

// https://chain.api.btc.com/v3/block/3
export async function getBlockByHeight(height: string, res: Response) {
	try {
		const resp: IBlockResp = await fetchUrl(`${chainApiBase}block/${height.toString()}`)
		console.log('block height response: ', resp)
		if (resp.err_no > 0 || resp.message !== 'success') { return }
		res.send(resp.data)
	} catch (error) {
		console.error(error)
		res.send({ error: error })
	}
}

// https://chain.api.btc.com/v3/block/latest
export async function getLatestBlock(res: Response) {
	try {
		const resp: IBlockResp = await fetchUrl(`${chainApiBase}block/latest`)
		console.log('block height response: ', resp)
		if (resp.err_no > 0 || resp.message !== 'success') { return }
		res.send(resp.data)
	} catch (error) {
		console.error(error)
		res.send({ error: error })
	}
}

// Get block list on 12/15/2015
// https://chain.api.btc.com/v3/block/date/20151215
export async function getBlockListByDate(date: string, res: Response) {
	try {
		const resp: IBlockListResp = await fetchUrl(`${chainApiBase}block/date/${date}`)
		if (resp.err_no > 0 || resp.message !== 'success') { return }
		res.send(resp.data)
	} catch (error) {
		console.error(error)
		res.send({ error: error })
	}
}

// https://chain.api.btc.com/v3/block/latest/tx
export async function getLatestBlockTransactions(res: Response) {
	try {
		const resp: ITransactionsList = await fetchUrl(`${chainApiBase}block/latest/tx`)
		if (resp.err_code !== 200) { return }
		res.send(resp.data)
	} catch (error) {
		console.error(error)
		res.send({ error: error })
	}
}

// https://chain.api.btc.com/v3/tx/0eab89a271380b09987bcee5258fca91f28df4dadcedf892658b9bc261050d96
export async function getTxInfosByTxHash(hash: string, res: Response) {
	try {
		const resp: ITransactionInfos = await fetchUrl(`${chainApiBase}tx/${hash}`)
		if (resp.err_no > 0 || resp.message !== 'success') { return }
		res.send(resp.data)
	} catch (error) {
		console.error(error)
		res.send({ error: error })
	}
}

// https://chain.api.btc.com/v3/address/15urYnyeJe3gwbGJ74wcX89Tz7ZtsFDVew
export async function getAddressInfos(address: string, res: Response) {
	try {
		const resp: IAddressInfosResp = await fetchUrl(`${chainApiBase}address/${address}`)
		if (resp.err_no > 0 || resp.message !== 'success') { return }
		res.send(resp.data)
	} catch (error) {
		console.error(error)
		res.send({ error: error })
	}
}

// https://chain.api.btc.com/v3/address/15urYnyeJe3gwbGJ74wcX89Tz7ZtsFDVew/tx
export async function getAddressTxs(address: string, res: Response) {
	try {
		const resp: ITransactionsList = await fetchUrl(`${chainApiBase}address/${address}/tx`)
		if (resp.err_no > 0 || resp.message !== 'success') { return }
		res.send(resp.data)
	} catch (error) {
		console.error(error)
		res.send({ error: error })
	}
}

// source : https://blockchain.info/
const bcInfoBase = 'https://blockchain.info/'

// https://blockchain.info/q/totalbc
export async function getTotalBtc(res: Response) {
	try {
		const resp: number = await fetchUrl(`${bcInfoBase}q/totalbc`)
		if (!isNum(resp)) { return }
		res.send({ response: resp })
	} catch (error) {
		console.error(error)
		res.send({ error: error })
	}
}

// https://blockchain.info/q/probability
export async function getOneHashProbability(res: Response) {
	try {
		const resp: number = await fetchUrl(`${bcInfoBase}q/probability`)
		if (!isNum(resp)) { return }
		res.send({ response: resp })
	} catch (error) {
		console.error(error)
		res.send({ error: error })
	}
}

// https://blockchain.info/q/hashestowin
export async function getHashesNeededToWin(res: Response) {
	try {
		const resp: number = await fetchUrl(`${bcInfoBase}q/hashestowin`)
		if (!isNum(resp)) { return }
		res.send({ response: resp })
	} catch (error) {
		console.error(error)
		res.send({ error: error })
	}
}

// https://blockchain.info/q/nextretarget
export async function getBlockWhereDiffRetargets(res: Response) {
	try {
		const resp: number = await fetchUrl(`${bcInfoBase}q/nextretarget`)
		if (!isNum(resp)) { return }
		res.send({ response: resp })
	} catch (error) {
		console.error(error)
		res.send({ error: error })
	}
}