import { createNodeRedisClient } from 'handy-redis'

export const redisClient = () => createNodeRedisClient(process.env.REDIS_URL || '')