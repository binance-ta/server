import { IRecClub } from '../models/interfaces/vps'
import { redisClient } from './redis'
import { getClubSeasonStats } from 'proclubs-api/dist/core/club'
import { TPlatformType } from 'proclubs-api/dist/core/api'
import { WrappedNodeRedisClient } from 'handy-redis'

export const updateRecord = async () => {

	console.log('start update record clubs...')

	const client = redisClient()
	if (!client) { return }

	const updatedPromiseArr = []

	// get all current records
	const recsStr = await Promise.all([
		client.get('ps4-record'),
		client.get('ps5-record'),
		client.get('xboxone-record'),
		client.get('xbox-series-xs-record'),
		client.get('pc-record')
	])

	// parse all current records
	const ps4Recs: IRecClub[] = JSON.parse(recsStr[0] || '[]')
	const ps5Recs: IRecClub[] = JSON.parse(recsStr[1] || '[]')
	const xb1Recs: IRecClub[] = JSON.parse(recsStr[2] || '[]')
	const xbsxsRecs: IRecClub[] = JSON.parse(recsStr[3] || '[]')
	const pcRecs: IRecClub[] = JSON.parse(recsStr[4] || '[]')

	// updated records after fetch
	const ps4UpdatedRecs: IRecClub[] = []
	const ps5UpdatedRecs: IRecClub[] = []
	const xb1UpdatedRecs: IRecClub[] = []
	const xbsxsUpdatedRecs: IRecClub[] = []
	const pcUpdatedRecs: IRecClub[] = []

	// fetch ps4 clubs
	if (ps4Recs.length > 0) {
		// await fetch clubs then push updated club array
		await fetchClubs(ps4Recs, ps4UpdatedRecs, 'ps4')
		// write new recs after loop fetches
		updatedPromiseArr.push(client.set('ps4-record', JSON.stringify(ps4UpdatedRecs)))
	}

	// fetch ps5 clubs
	if (ps5Recs.length > 0) {
		// await fetch clubs then push updated club array
		await fetchClubs(ps5Recs, ps5UpdatedRecs, 'ps5')
		// write new recs after loop fetches
		updatedPromiseArr.push(client.set('ps5-record', JSON.stringify(ps5UpdatedRecs)))
	}

	// fetch xb1 clubs
	if (xb1Recs.length > 0) {
		// await fetch clubs then push updated club array
		await fetchClubs(xb1Recs, xb1UpdatedRecs, 'xboxone')
		// write new recs after loop fetches
		updatedPromiseArr.push(client.set('xboxone-record', JSON.stringify(xb1UpdatedRecs)))
	}

	// fetch xbsxs clubs
	if (xbsxsRecs.length > 0) {
		// await fetch clubs then push updated club array
		await fetchClubs(xbsxsRecs, xbsxsUpdatedRecs, 'xbox-series-xs')
		// write new recs after loop fetches
		updatedPromiseArr.push(client.set('xbox-series-xs-record', JSON.stringify(xbsxsUpdatedRecs)))
	}

	// fetch pc clubs
	if (pcRecs.length > 0) {
		// await fetch clubs then push updated club array
		await fetchClubs(pcRecs, pcUpdatedRecs, 'pc')
		// write new recs after loop fetches
		updatedPromiseArr.push(client.set('pc-record', JSON.stringify(pcUpdatedRecs)))
	}

	/* console.log('ps4UpdatedRecs: ', ps4UpdatedRecs)
	console.log('ps5UpdatedRecs: ', ps5UpdatedRecs)
	console.log('xb1UpdatedRecs: ', xb1UpdatedRecs)
	console.log('xbsxsUpdatedRecs: ', xbsxsUpdatedRecs)
	console.log('pcUpdatedRecs: ', pcUpdatedRecs) */

	// write updated clubs
	await Promise.all(updatedPromiseArr)

	const allClubs = [...ps4Recs, ...ps5Recs, ...xb1Recs, ...xbsxsRecs, ...pcRecs]

	getHighestRec(allClubs, client)

	await client.quit()
}

const fetchClubs = async (platformRecs: IRecClub[], platUpdatedRecs: IRecClub[], plat: TPlatformType) => {
	console.log(`start fetching ${plat} clubs now...`)
	if (platformRecs.length > 0) {
		// console.log(`${plat} record clubs: `, platformRecs)
		try {
			for (const club of platformRecs) {
				// only fetch if club has no losses
				if (!club.hasLost) {
					const fetchedClub = await getClubSeasonStats(plat, +club.clubId)
					// only update if club is not deleted and still has no losses
					if (fetchedClub && fetchedClub.losses === '0') {
						platUpdatedRecs.push({
							...fetchedClub,
							clubInfo: club.clubInfo,
							hasLost: false,
							squadList: [],
							addedAt: club.addedAt,
							name: club.name,
							plat
						})
					} else {
						console.log(`club: ${club.name} has been deleted, write the already available club...`)
						platUpdatedRecs.push({
							...club,
							hasLost: true,
							squadList: [],
							addedAt: club.addedAt,
							name: club.name,
							plat
						})
					}
				} else {
					console.log(`club: ${club.name} has lost, write the already available club...`)
					platUpdatedRecs.push({
						...club,
						hasLost: true,
						squadList: [],
						addedAt: club.addedAt,
						name: club.name,
						plat
					})
				}
			}
		} catch (e) {
			console.log(`error while fetching ${plat} clubs`)
		}
	}
}

export const getHighestRec = async (
	allRecClubs: IRecClub[],
	client: WrappedNodeRedisClient
) => {

	console.log('getting highest record')

	const highest: IRecClub[] = []

	// TODO sort by points?
	// need to get the highest points from array
	// it is possible that multiple clubs have the same points so we return them all
	// otherwise we return the highest one

	// sort array from highest record to lowest
	allRecClubs.sort((a, b) => {
		if (+a.wins === +b.wins && +a.ties > +b.ties) { return -1 }
		if (+a.wins > +b.wins) { return -1 }
		if (+a.wins === +b.wins && +a.ties === +b.ties) { return 0 }
		return 1
	})

	highest.push(allRecClubs[0])

	// bad solution (taking only max 3 same entries)
	/* if (allRecClubs[0].points === allRecClubs[1].points) {
		highest.push(allRecClubs[1])
		if (allRecClubs[0].points === allRecClubs[2].points) {
			highest.push(allRecClubs[2])
		}
	} */

	console.log('highest: ', highest[0].name)
	console.log('record: ', highest[0].wins)
	// const max = allRecClubs.reduce((prev, current) => (+prev.wins < +current.wins && +prev.points < +current.points) ? prev : current)
	// console.log('max rec: ', max)
	await client.set('highest-record', JSON.stringify(highest))
}

export const resetRedis = async () => {
	const client = redisClient()
	if (!client) { return }

	await client.flushall()
	await Promise.all([
		client.set('AT', '251'),
		client.set('ps4-record', '[]'),
		client.set('ps5-record', '[]'),
		client.set('xboxone-record', '[]'),
		client.set('xbox-series-xs-record', '[]'),
		client.set('pc-record', '[]')
	])

	await client.quit()
}